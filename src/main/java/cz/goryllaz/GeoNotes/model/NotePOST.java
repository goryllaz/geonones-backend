package cz.goryllaz.GeoNotes.model;

public class NotePOST {
    private String text;
    private Double latitude;
    private Double longitude;

    public NotePOST() {
    }

    public NotePOST(String text, Double latitude, Double longitude) {
        this.text = text;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
