package cz.goryllaz.GeoNotes.model;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity(name = "NOTES")
public class Note {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "uuid-char")
    @Column(name = "ID_NOTE", columnDefinition = "VARCHAR2(40)")
    private UUID idNote;

    @Lob
    @Column(name = "TEXT", columnDefinition = "CLOB")
    private String text;

    @Column(name = "LATITUDE", columnDefinition = "NUMBER")
    private Double latitude;

    @Column(name = "LONGITUDE", columnDefinition = "NUMBER")
    private Double longitude;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE_CREATE", columnDefinition = "DATE")
    @DateTimeFormat(pattern="dd-MM-yyyy HH:mm")
    private Date dateCreate;

    @ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="ID_USER")
    private User user;

    public Note() {

    }

    public Note(String text, Double latitude, Double longitude, Date dateCreate, User user) {
        this.text = text;
        this.latitude = latitude;
        this.longitude = longitude;
        this.dateCreate = dateCreate;
        this.user = user;
    }

    public UUID getIdNote() {
        return idNote;
    }

    public void setIdNote(UUID idNote) {
        this.idNote = idNote;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
