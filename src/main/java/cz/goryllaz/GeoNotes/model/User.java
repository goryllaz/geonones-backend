package cz.goryllaz.GeoNotes.model;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity(name = "USERS")
public class User {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "uuid-char")
    @Column(name = "ID_USER", columnDefinition = "VARCHAR2(40)")
    private UUID idUser;

    @Column(name = "USERNAME", columnDefinition = "VARCHAR2(40)")
    private String username;

    @Column(name = "TOKEN", columnDefinition = "VARCHAR2(60)")
    private String token;

    @OneToMany(mappedBy = "user")
    private List<Note> notes;

    public User() {
    }

    public User(String username, String token) {
        this.username = username;
        this.token = token;
    }

    public UUID getIdUser() {
        return idUser;
    }

    public void setIdUser(UUID idUser) {
        this.idUser = idUser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
