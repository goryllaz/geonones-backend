package cz.goryllaz.GeoNotes.model;

import java.util.Date;

public class NoteTMP {
    private String id;
    private String text;
    private Double latitude;
    private Double longitude;
    private Date dateCreate;
    private String username;

    public NoteTMP(String id, String text, Double latitude, Double longitude, Date dateCreate, String username) {
        this.id = id;
        this.text = text;
        this.latitude = latitude;
        this.longitude = longitude;
        this.dateCreate = dateCreate;
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
