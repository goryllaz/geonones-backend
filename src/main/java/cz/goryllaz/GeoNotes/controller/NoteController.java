package cz.goryllaz.GeoNotes.controller;

import cz.goryllaz.GeoNotes.model.Note;
import cz.goryllaz.GeoNotes.model.NotePOST;
import cz.goryllaz.GeoNotes.model.NoteTMP;
import cz.goryllaz.GeoNotes.repository.NoteRepository;
import cz.goryllaz.GeoNotes.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class NoteController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private NoteRepository noteRepository;


    @PostMapping("/app/addnote")
    public ResponseEntity<NoteTMP> addNote(@RequestBody NotePOST notePOST){
        Note note = new Note(
                notePOST.getText(),
                notePOST.getLatitude(),
                notePOST.getLongitude(),
                new Date(),
                userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName())
        );

        noteRepository.save(note);

        return new ResponseEntity<>(new NoteTMP(note.getIdNote().toString(), note.getText(), note.getLatitude(), note.getLongitude(), note.getDateCreate(), note.getUser().getUsername()), HttpStatus.OK);
    }

    @GetMapping("/app/getnotes")
    public List<NoteTMP> getNote(@RequestParam(name = "latitude") double latitude, @RequestParam(name = "longitude") double logitude) {
        List<NoteTMP> notes = new ArrayList();
        double dist = 200.0;
        noteRepository.findAllByLatitudeBetweenAndLongitudeBetween(
                meterToLatitudeMin(dist, latitude),
                meterToLatitudeMax(dist, latitude),
                meterToLongitudeMin(dist, logitude, latitude),
                meterToLongitudeMax(dist, logitude, latitude)
        ).stream().forEach(note -> notes.add(new NoteTMP(
                note.getIdNote().toString(),
                note.getText(),
                note.getLatitude(),
                note.getLongitude(),
                note.getDateCreate(),
                note.getUser().getUsername()
        )));
        return notes;
    }

    private double meterToLatitudeMax(final double dist, final double lat) {
        final double radius = 6371000.0;
        return lat + Math.toDegrees(dist / radius);
    }

    private double meterToLatitudeMin(final double dist, final double lat) {
        final double radius = 6371000.0;
        return lat - Math.toDegrees(dist / radius);
    }

    private double meterToLongitudeMax(final double dist, final double lng, final double lat) {
        final double radius = 6371000.0;
        return lng + Math.toDegrees(dist / radius / Math.cos(Math.toRadians(lat)));
    }

    private double meterToLongitudeMin(final double dist, final double lng, final double lat) {
        final double radius = 6371000.0;
        return lng - Math.toDegrees(dist / radius / Math.cos(Math.toRadians(lat)));
    }

}
