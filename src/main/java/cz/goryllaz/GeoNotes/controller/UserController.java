package cz.goryllaz.GeoNotes.controller;

import cz.goryllaz.GeoNotes.exception.UserExistException;
import cz.goryllaz.GeoNotes.model.User;
import cz.goryllaz.GeoNotes.model.UserTMP;
import cz.goryllaz.GeoNotes.repository.UserRepository;
import cz.goryllaz.GeoNotes.security.TokenGenerator;
import cz.goryllaz.GeoNotes.service.AddUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddUserService addUserService;

    @PostMapping("/nicetomeetyou")
    public UserTMP registration(@RequestParam(name = "username") String username) {
        if (userRepository.findByUsername(username) == null) {
            String token = TokenGenerator.generateToken();
            User user = new User(username, token);
            addUserService.saveUser(user);
            return new UserTMP(username, token);
        } else throw new UserExistException(username);
    }

    /* Test auth TODO Odstranit */
    @GetMapping("/app/getid")
    public String getUserId() {
        return userRepository.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        ).getIdUser().toString();
    }
}
