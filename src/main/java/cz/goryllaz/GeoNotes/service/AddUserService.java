package cz.goryllaz.GeoNotes.service;

import cz.goryllaz.GeoNotes.model.User;

public interface AddUserService {
    void saveUser(User user);
}
