package cz.goryllaz.GeoNotes.service;

public interface LoginService {
    String findLoggedInUsername();

    boolean isLoggedIn();
}
