package cz.goryllaz.GeoNotes.service;

import cz.goryllaz.GeoNotes.model.User;
import cz.goryllaz.GeoNotes.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AddUserServiceImpl implements AddUserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void saveUser(User user) {
        user.setToken(bCryptPasswordEncoder.encode(user.getToken()));
        userRepository.save(user);
    }
}
