package cz.goryllaz.GeoNotes.repository;

import cz.goryllaz.GeoNotes.model.Note;
import cz.goryllaz.GeoNotes.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface NoteRepository  extends JpaRepository<Note, UUID> {
    List<Note> findAllByUser_Username(String username);
    List<Note> findAllByLatitudeBetweenAndLongitudeBetween(double latMin, double latMax, double longMin, double longMax);
}
