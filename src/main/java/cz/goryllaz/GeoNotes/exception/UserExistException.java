package cz.goryllaz.GeoNotes.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class UserExistException extends RuntimeException {
    public UserExistException(String username) {
        super("Username " + username + " already exist");
    }
}
