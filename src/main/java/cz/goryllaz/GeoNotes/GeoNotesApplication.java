package cz.goryllaz.GeoNotes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeoNotesApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeoNotesApplication.class, args);
	}

}
